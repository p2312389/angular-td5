import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { MatIcon } from '@angular/material/icon';
import { NgOptimizedImage } from '@angular/common';
import { Page } from '../../models/page.model';

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [
    MatIcon,
    NgOptimizedImage
  ],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input({ required: true }) vol!: Vol;
  @Input() page: Page = Page.DECOLLAGE
  protected readonly Page = Page;
}
