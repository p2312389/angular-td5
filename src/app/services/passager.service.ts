import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { IPassagerDto, Passager } from '../models/passager.model';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor(private http: HttpClient) {
  }

  /**
   * Récupération de la liste des passagers pour un vol
   * @param icaoVol le numéro icao du vol concerné
   * Documentation : https://randomuser.me/
   */
  getPassagers(icaoVol: string): Observable<Passager[]> {
    return this.http.get<any>(`https://randomuser.me/api?inc=name,picture,email&seed=${icaoVol}&results=20`).pipe(
      map((response) => response['results']
        .map((dto: IPassagerDto) => new Passager(dto))
      ));
  }
}
